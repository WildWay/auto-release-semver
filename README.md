# Автотегирование semver приложений

[[_TOC_]]

## [Gitlab getting started](.readme.d/gitlab_getting_started.md)

## Полезные ссылки
1. [semver на русском][semver на русском]
2. [semver на английском][semver на английском] - больше информации, шаблоны regexp
3. [GitLab predefined variables][GitLab predefined variables]
4. [Проверка regexp с шаблоном текущего пайплайна][Проверка regexp с шаблоном текущего пайплайна]
5. [Проверка regexp с шаблоном тега][Проверка regexp с шаблоном тега]

[semver на русском]: https://semver.org/lang/ru/
[semver на английском]: https://semver.org
[GitLab predefined variables]: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
[Проверка regexp с шаблоном текущего пайплайна]: https://regex101.com/r/Mm8tmO/4
[Проверка regexp с шаблоном тега]: https://regex101.com/r/8rSQPo/2

## Логика работы:

### Шаг 1 - check_release_info
В любой ветке отрабатывает операция (job) **check_release_info**, в которой производится проверка имени ветки на соответствие [условиям regexp][Проверка regexp с шаблоном текущего пайплайна] для последующего тегирования:
- regexp - `^(major|minor|patch)\/[a-zA-Z]+[a-zA-Z0-9\/_-]*[a-zA-Z0-9]+$`
- пояснение regexp:
    1. имя ветки должно начинаться с одного из слов:
        - **"major"**
        - **"minor"**
        - **"patch"**
    1. после должна идти черта **"/"**
    1. затем **```^[a-zA-Z]+[a-zA-Z0-9\/_-]*[a-zA-Z0-9]+$```**
- Пример: **"minor/some_new_feature"**

### Шаг 2 - мёрдж в основную ветку
При мёрдже в основную ветку (**CI_DEFAULT_BRANCH**) запускаются следующие операции:
1. **generate_release_info** - подготовка переменных, для последующего использования в **create_release**
1. **create_release** - создание тега и релиза

### Шаг 3 - generate_release_info
**generate_release_info** создаёт номер версии согласно одному из слов "major", "minor", "patch", а именно:
- если **тегов не было** - инициализируется переменная **LATEST_TAG=0.0.0**
- если **был** хотя бы один **тег** - он **проверяется на соответствие** [условиям regexp][Проверка regexp с шаблоном тега] `^([0-9]+\.){2}[0-9]+$`
    - при соответствии - обрезается до формата трёх цифр через точку, присваивается переменной TAG
    - при несоответствии выдаётся ошибка с рекомендациями
- переменная **LATEST_TAG разбивается по формату semver** {MAJOR}.{MINOR}.{PATCH}
- **генериируется переменная TAG** исходя из слов "major", "minor", "patch":
    - "major" - `$((MAJOR+1)).0.0`
    - "minor" - `{MAJOR}.$((MINOR+1)).0`
    - "patch" - `{MAJOR}.{MINOR}.$((PATCH+1))`

### Шаг 4 - create_release
**create_release** создаёт тег и релиз согласно версии в переменной TAG. К примеру, **если TAG=0.0.0**:
- Для ветки "major" будет **1.0.0**
- Для ветки "minor" будет **0.1.0**
- Для ветки "patch" будет **0.0.1**

## Необходимые настройки репозитория
В настройках репозитория для default ветки обязательно должен быть запрещён push.
Settings => Repository => Protected branches => Allowed to push = "No one".

Это необходимо во избежание случайного пуша в default ветку и как следствие запуска текущего пайплайна автотегирования.
В community edition (бесплатной версии) более изящных альтернатив не обнаружено.
